from django.urls import path
from home import views


app_name ="home"
urlpatterns = [
    path('', views.Index.as_view(), name="index"),
    path('signup/', views.SignUp.as_view(), name="signup"),
    path('logout/', views.LogOut.as_view(), name="logout"),
    path('about/', views.About.as_view(), name="about"),
    path('user_profile/', views.UserProfile.as_view(), name="user_profile"),
    path('identify/', views.Indentify.as_view(), name="identify"),
    path('contact/', views.contact.as_view(), name="contact"),
]