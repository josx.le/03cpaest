from django.forms.models import BaseModelForm
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from .forms import LoginForm, SignUpForm, UserProfileForm
from .models import UserProfile

# Create your views here.

class Index(generic.View):
    template_name = "home/index.html"
    context = {}
    form = LoginForm()

    def get(self, request):
        self.context = {
            "name": "José López",
            "lista": [1, 2, 3],
            "form": self.form
        }
        return render(request, self.template_name, self.context)
    
    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("/")
        else:
            return redirect("/")

    
class LogOut(generic.View):
    def get(self, request):
        logout(request)
        return redirect("/")
    

class SignUp(generic.CreateView):
    template_name = "home/signup.html"
    form_class = SignUpForm
    success_url = reverse_lazy("home:index")

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get("username")
        password1 = form.cleaned_data.get("password1")
        user = authenticate(self.request, username=username, password=password1)
        if user is not None:
            login(self.request, user)
        return redirect("/")




class UserProfile(LoginRequiredMixin, generic.CreateView):
    template_name = "home/user_profile.html"
    model = UserProfile
    form_class = UserProfileForm
    success_url = reverse_lazy("home:user_profile")
    login_url = "/"



class About(generic.View):
    template_name = "home/about.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "josé lópez"
        }
        return render(request, self.template_name, self.context)


class Identity(generic.View):
    template_name = "home/identity.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "José López"
        }
        return render(request, self.template_name, self.context)


class Contact(generic.View):
    template_name = "home/contact.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "José"
        }
        return render(request, self.template_name, self.context)

    

class About(generic.View):
    template_name = "home/about.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "José López"
        }
        return render(request, self.template_name, self.context)
     


class Indentify(generic.View):
    template_name = "home/identify.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "José López"
        }
        return render(request, self.template_name, self.context)
    


class contact(generic.View):
    template_name = "home/contact.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "José López"
        }
        return render(request, self.template_name, self.context)
    
    
