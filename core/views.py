from django.shortcuts import render

from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin

from django.urls import reverse_lazy
from .models import GrantGoal
from .forms import GrantGoalForm, UpdateGrantGoalForm
# Create your views here.

##### G R A N T G O A L C R U D #####

##Create
class CreateGrantGoal(LoginRequiredMixin, generic.CreateView):
    template_name = "core/gg_create.html"
    model = GrantGoal
    form_class = GrantGoalForm
    success_url = reverse_lazy("core:gg_list")
    login_url = reverse_lazy("home:index")


## Retrieve
#List GrantGoal
class ListGrantGoal(LoginRequiredMixin, generic.View):
    template_name = "core/gg_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = GrantGoal.objects.filter(status=True)
        self.context = {
            "grant_goals": queryset
        }
        return render(request, self.template_name, self.context)


#Detail
class DetailGrantGoal(LoginRequiredMixin, generic.DetailView):
    template_name = "core/gg_detail.html"
    model = GrantGoal
    login_url = reverse_lazy("home:index")


#Update
class UpdateGrantGoal(LoginRequiredMixin, generic.UpdateView):
    template_name = "core/gg_update.html"
    model = GrantGoal
    form_class = UpdateGrantGoalForm
    success_url = reverse_lazy("core:gg_list")
    login_url = reverse_lazy("home:index")


#Delete
class DeleteGrantGoal(LoginRequiredMixin, generic.DeleteView):
    template_name = "core/gg_delete.html"
    model = GrantGoal
    success_url = reverse_lazy("core:gg_list")
    login_url = reverse_lazy("home:index")

